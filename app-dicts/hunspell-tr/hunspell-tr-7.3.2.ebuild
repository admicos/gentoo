# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

# bashism
_ver="${PV//./-}"

DESCRIPTION="Turkish Hunspell Dictionary"
HOMEPAGE="https://cgit.freedesktop.org/libreoffice/dictionaries/tree/tr_TR"
LICENSE="MPL-2.0"
SRC_URI="
https://cgit.freedesktop.org/libreoffice/dictionaries/plain/tr_TR/tr_TR.dic?h=libreoffice-${_ver} -> tr_TR.dic
https://cgit.freedesktop.org/libreoffice/dictionaries/plain/tr_TR/tr_TR.aff?h=libreoffice-${_ver} -> tr_TR.aff
"

RESTRICT="mirror" # disable mirrors
SLOT="0"
KEYWORDS="~amd64"
RDEPEND=">=app-text/hunspell-0.7.0-r2"
S="${WORKDIR}" # don't assume we're extracting any tars with a folder in them

src_unpack() {
	cd "${DISTDIR}" || die; # distdir is the folder with all the downloaded sources
	cp ${A} "${S}" || die;  # move all files downloaded via SRC_URI to where Portage looks for in doins ($A is relative to $DISTDIR?)
}

src_install() {
	insinto /usr/share/hunspell
	doins tr_TR.*
}
