#!/bin/sh -eu
echo Checking Updates...

find . -name updatechk.sh | while read -r script; do
    package="$(dirname "$script")"
    pkg_name="$(basename "$package")"
    cd "$package"

    current_version="$(find -- *.ebuild | sed -e "s/.ebuild$//" -e "s/$pkg_name-//")"
    latest_version="$(./updatechk.sh)"

    if [ "$current_version" != "$latest_version" ]; then
        printf "%s OUTDATED\n  current: %s, latest: %s\n" "$package" "$current_version" "$latest_version"
    else
        printf "%s UP TO DATE\n" "$package"
    fi

    cd "../../"
done